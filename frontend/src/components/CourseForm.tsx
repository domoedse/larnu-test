import { useState } from 'react'
import {
    Text,
    Textarea,
    Container,
    Group,
    Button,
    Modal,
    Center,
    Input,
    useMantineTheme,
    Loader

} from '@mantine/core'
import { IconUpload, IconPhoto, IconX } from '@tabler/icons';
import { Dropzone, DropzoneProps, IMAGE_MIME_TYPE } from '@mantine/dropzone';

interface Courses {
    renderCourses: Function;
    modalControl: Function;
}
export default function AddCourse(props: (Partial<DropzoneProps> & Courses)) {
    const [loadingModal, setLoadingModal] = useState<boolean>(false);
    const [courseTitle, setCourseTitle] = useState<string>();
    const [courseContent, setCourseContent] = useState<string>();
    const [base64, setBase64] = useState<string>();
    const theme = useMantineTheme();
    const onChange = (e: any) => {
        const files = e;
        const file = files[0];
        getBase64(file);
    };
    const onLoad = (fileString: any) => {
        setBase64(fileString)
    };

    const getBase64 = (file: any) => {
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => {
            onLoad(reader.result);
        };
    };
    const onFileSubmit = async (e: any) => {
        setLoadingModal(true)
        const postData = {
            title: courseTitle,
            content: courseContent,
            authorEmail: "larnutest@larnu.com",
            imgSrc: base64,
        }
        try {
            const res = await fetch(`http://localhost:3111/course`, {
                method: "post",
                headers: {
                    "Content-Type": "application/json",
                    "x-access-token": "token-value",
                },
                body: JSON.stringify(postData),
            });
            if (!res.ok) {
                const message = `An error has occured: ${res.status} - ${res.statusText}`;
                throw new Error(message);
            }
            const codeRes = await res.status;
            if(codeRes===200){
                setLoadingModal(false)
                props.renderCourses(true)
                props.modalControl(false)
            }
        } catch (err: any) {
            console.log(err.message);
        }
    }
    return (
        <Container size={500} px="md">
            <Modal
                opened={loadingModal}
                onClose={() => setLoadingModal(false)}
                title={'Cargando...'}
                centered
                transition={'skew-up'}
                transitionDuration={700}
            >
                <Center><Loader color="grape" size="lg" variant="bars" /></Center>
                
            </Modal>
            <form>
                <Center><Text>Crea un Curso</Text></Center>
                <Input placeholder='Nombre del curso' required onChange={(evnt: any)=>setCourseTitle(evnt.target.value)}></Input>
                <Dropzone
                    onDrop={(files) => onChange(files)}
                    onReject={(files) => console.log('rejected files', files)}
                    maxSize={(1 / 5) * 1024 ** 2}
                    accept={IMAGE_MIME_TYPE}
                    {...props}
                >
                    <Group position="center" spacing="xl" style={{ minHeight: 220, pointerEvents: 'none' }}>
                        <Dropzone.Accept>
                            <IconUpload
                                size={50}
                                stroke={1.5}
                                color={theme.colors[theme.primaryColor][theme.colorScheme === 'dark' ? 4 : 6]}
                            />
                        </Dropzone.Accept>
                        <Dropzone.Reject>
                            <IconX
                                size={50}
                                stroke={1.5}
                                color={theme.colors.red[theme.colorScheme === 'dark' ? 4 : 6]}
                            />
                        </Dropzone.Reject>
                        <Dropzone.Idle>
                            <IconPhoto size={50} stroke={1.5} />
                        </Dropzone.Idle>

                        <div>
                            <Text size="xl" inline>
                                Arrastra la imagen del curso aquí.
                            </Text>
                            <Text size="sm" color="dimmed" inline mt={7}>
                                Tu imagen no debe exceder de 200 Kbytes.
                            </Text>
                        </div>
                    </Group>
                </Dropzone>
                <Center>
                    <Textarea
                        placeholder="Contenido del curso"
                        label="Contenido del curso"
                        required
                        autosize
                        minRows={4}
                        maxRows={12}
                        onChange={(evnt: any)=>setCourseContent(evnt.target.value)}
                    />
                </Center>
                <Center>
                    <Button onClick={onFileSubmit}>Guardar</Button>
                </Center>

            </form>
        </Container>);
}