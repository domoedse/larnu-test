import { useState, useEffect } from 'react'
import CourseCard from './CourseCard'
import AddCourse from './CourseForm'
import { Grid, Center, Button, Modal  } from '@mantine/core'

type ResponseAPI = {
    "id": number,
    "title": string,
    "content": string,
    "imgSrc": string, 
    "published": true,
    "authorId": number,
    "author": {
        "id": number,
        "email": string,
        "name": string
    }
}
export default function Cursos () {
    const [ cursos, setCursos ] = useState<any>()
    const [ opened, setOpened] = useState<boolean>(false);
    const [ renderCourses, setRenderCourses ] = useState<boolean>(false);
    useEffect(()=>{
        var arrAuxCardCourse: Array<any> = []
        fetch('http://localhost:3111/feed').then(data=>data.json()).then(response=>{
            response.forEach((curso: ResponseAPI, index: number)=>{
                arrAuxCardCourse.push((<CourseCard idnt={curso.id} image={curso.imgSrc} title={curso.title} content={curso.content} country='Remoto'  renderCourses={setRenderCourses}/>))
                if(index+1 === response.length){
                    setCursos(Array.from(new Set(arrAuxCardCourse.map(course=><Grid.Col xs={12} sm={12} md={6} lg={4} xl={3}>{course}</Grid.Col>))))
                }
            });
        })
    }, [renderCourses])
    return (
    <>
    <Modal
        opened={opened}
        onClose={() => setOpened(false)}
        centered
        transition={'skew-down'}
        transitionDuration={700}
    >
        <AddCourse renderCourses={setRenderCourses} modalControl={setOpened}/>
    </Modal>
        <Center><Grid align={'center'} gutter="xs">{cursos!}</Grid></Center>
        <Center><Button onClick={()=> setOpened(true)}>Agregar Curso</Button></Center>
    </>
    )
}